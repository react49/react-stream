import React  from 'react';
import { Link } from 'react-router-dom';

import GoogleAuth from './GoogleAuth';

const Header = () => {
    return (
        <div className="ui secondary pointing menu" style={{ marginTop: "10px" }}>
            <Link className="item" to="/">
                <h3> <i className="camera retro icon large"></i> Streamers</h3>
                </Link>

            <div className="right menu">
                <Link className="item" to="/" style={{ paddingBottom: '25px' }}>All Streams</Link>
                <GoogleAuth />
            </div>
        </div>
    )
}

export default Header;