import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { fetchStreams } from '../../actions';

class StreamList extends React.Component {

    componentDidMount() {
        this.props.fetchStreams();
    }

    renderAdminButton(stream) {
        if(this.props.isSignedIn){
            if(stream.userId === this.props.currentUserId){
                return (
                    <div className="right floated content">
                        <Link to={`/streams/edit/${stream.id}`} className="ui tiny button primary">EDIT</Link>
                        <Link to={`/streams/delete/${stream.id}`} className="ui tiny button negative">DELETE</Link>
                    </div>
                );
            }
        }
    }

    renderList() {
        return this.props.streams.map(stream => {
            return (
                <div className="item" key={stream.id} style={{ borderLeft: "1px solid #DEDEDE", borderRight: "1px solid #DEDEDE" }}>
                    {this.renderAdminButton(stream)}

                    <i className="large middle aligned icon camera"></i>
                    <div className="content">
                        <Link to={`/streams/${stream.id}`} className="header">{stream.title}</Link>

                        <div className="description">
                            {stream.description}
                        </div>
                    </div>

                </div>
            );
        });
    };

    renderCreateButton() {
        if(this.props.isSignedIn){
            return (
                <div style={{ textAlign: 'right' }}>
                    <Link to="/streams/new" className="ui button green">Create Stream</Link>
                </div>
            )
        }
    }

    render() {
        return (
            <div>
                <div className="ui two column grid">
                    <div className="row">
                        <div className="column">
                            <h2>Streams</h2>
                        </div>
                        <div className="column">
                            {this.renderCreateButton()}
                        </div>
                    </div>
                </div>


                <div className="ui celled list">
                    {this.renderList()}
                </div>
            </div>
        );
    };

}

const mapStateToProps = (state) => {
    return {
        streams: Object.values(state.streams),
        currentUserId: state.auth.userId,
        isSignedIn: state.auth.isSignedIn
    }
}

export default connect(mapStateToProps, {
    fetchStreams
})(StreamList);
