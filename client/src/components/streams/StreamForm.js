import React from 'react';
import { Field, reduxForm } from 'redux-form';

class StreamForm extends React.Component {

    renderError({ error, touched }) {
        if(touched && error) {
            return (
                <div className="ui error message">
                    <div className="header">{error}</div>
                </div>
            );
        }
    } 
    
    renderInput = ({ input, label, meta }) => {
        let className = 'field';
        if(meta.error && meta.touched){
            className = `field error`;
        }

        return (
            <div className={className}>
                <label>{label}</label>
                <input {...input} autoComplete="off" />

                {this.renderError(meta)}
            </div>
        )
    }

    onSubmit = (formValues) => {
        this.props.onSubmit(formValues);
    }
    
    render() {
        return (
            <div>
                <form className="ui form error" onSubmit={this.props.handleSubmit(this.onSubmit)}>
                    <Field name="title" label="Enter Title" component={this.renderInput} />
                    <Field name="description" label="Enter Descrition" component={this.renderInput} />

                    <button className="ui button primary">Submit</button>
                </form>
            </div>
        );
    }
}

// VALIDATE FUNCTION
const validate = (formValues) => {
    const errors = {};

    if(!formValues.title){
        errors.title = 'you must enter a title';
    }

    if(!formValues.description){
        errors.description = 'you must enter a description';
    }

    return errors;
}

export default reduxForm({
    form: 'streamForm',
    validate
})(StreamForm);
